---
title: "Hello World"
author:
  name: "Devon Ryder"
date: 2021-04-10T14:54:01-04:00
draft: false
toc: false
images:
tags:
  - general
  - tech
  - programming
---

## My Name is Devon Ryder

I am an experienced IT technician with a passion for Linux and developing my skills with regard to administration and development. After graduating as a Computer Programmer, I've worked as an IT technician for a small IT firm / MSP where we were faced with a large variety of issues daily. During this time I've developed valuable skills with regard to managing Linux cPanel and various Windows Servers, desktop support, ONVIF security camera configuration, FreePBX management, and much more.

For the last few months I've been unemployed looking for work. During this time I've tried to develop valuable skills to progress myself as a tech professional and enthusiast. Skills I've worked to develop include Python, Docker, Hugo, and various Linux concepts.

To be honest, I am not quite sure of the route I planned to go for this first post, so I figure I'll show a little about the setup I've been using daily, which runs Fedora 33 on a desktop ITX system.

### PC Setup

* Ryzen 5 2600
* 16 GB 3200 Corsair Vengeance LPX
* MSI RX 580 8 GB
* 500 GB WD Blue M.2
* ASUS ROG Strix B450i
* Corsair SF600 W
* Silverstone Raven RVZ03B

#### Peripherals

* Corsair K63 Wireless
* Logitech G603 Wireless
* Audio Technica ATH M60X
* (2) Dell U2515H (2560x1440)

![My Desk Setup](/img/desk.jpg "My Desk")

#### Software

* Fedora 33
* Visual Studio Code (coding)
* Vim (configs, etc)
* Docker + Compose
* Python + Django
* Hugo (this blog)

![My OS Setup](/img/neofetch.png "My OS")

I also dual boot Win 10, but it sees minimal use.

Due to the RX 580 GPU, graphics drivers are a breeze on Linux with AMD's well supported and performant AMDGPU kernel drivers.

### Goodbye for Now

That's it for now. I look forward to using this blog to outline some of the projects and tasks I am working on. You'll likely see Django + Docker and Linux content in the near future. Until then, goodbye, friend!
